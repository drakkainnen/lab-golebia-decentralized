import { AppController } from './app.controller';

export const appComponent: angular.IComponentOptions = {
  template: require('./app.html'),
  controller: AppController
};
