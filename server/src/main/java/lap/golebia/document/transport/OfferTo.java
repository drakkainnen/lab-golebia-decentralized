package lap.golebia.document.transport;

import java.util.List;

public class OfferTo {

	private String id;
	private BirdTo bird;
	private String username;
	private List<String> comments;

	public BirdTo getBird() {
		return bird;
	}

	public void setBird(BirdTo bird) {
		this.bird = bird;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<String> getComments() {
		return comments;
	}

	public void setComments(List<String> comments) {
		this.comments = comments;
	}

}
