package lap.golebia.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import lap.golebia.document.dao.CollectionEntity;

@Repository
public interface CollectionRepository extends JpaRepository<CollectionEntity, Long> {

	@Query("select collection from CollectionEntity collection " + "where collection.user.id = :userId")
	public Set<CollectionEntity> findByUser(@Param("userId") Long userId);

	@Query("select collection from CollectionEntity collection "
			+ "where collection.user.id = :userId "
			+ "and collection.id in :ids")
	public Set<CollectionEntity> findByIdsAndUser(@Param("userId") Long userId, @Param("ids") List<Long> ids);

	@Query("select collection from CollectionEntity collection "
			+ "where collection.user.id = :userId "
			+ "and collection.id = :id")
	public CollectionEntity findByIdAndUser(@Param("userId") Long userId, @Param("id") Long id);

	@Query("select collection from CollectionEntity collection "
			+ "inner join fetch collection.birds bird "
			+ "where collection.id in :ids "
			+ "and collection.user.id = :userId")
	public Set<CollectionEntity> findByIdsAndUserFetchBirds(@Param("userId") Long userId, @Param("ids") List<Long> ids);

}
