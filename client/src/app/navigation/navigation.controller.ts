import { AuthorizationService } from '../auth/authorization.service';

interface IPage {
    name: string;
    state: string;
    auth: string;
}

export class NavigationController {
    private pages: Array<IPage> = [
        {
            name: 'Home',
            state: 'app',
            auth: 'both'
        },
        {
            name: 'Birds',
            state: 'app.birds',
            auth: 'auth'
        },
        {
            name: 'Login',
            state: 'app.login',
            auth: 'no-auth'
        },
        {
            name: 'Logout',
            state: 'app.logout',
            auth: 'auth'
        },
        {
            name: 'Register',
            state: 'app.signup',
            auth: 'no-auth'
        },
        {
            name: 'User details',
            state: 'app.userdetails',
            auth: 'auth'
        }
    ];

    /* ng-inject */
    constructor(private authorizationService: AuthorizationService) {
    }

    getPermitedPages() {
        var that = this;
        return this.pages.filter(function (page: IPage) {
            if (page.auth === 'both') {
                return true;
            }
            if (that.authorizationService.authenticated) {
                return page.auth === 'auth';
            }
            return page.auth === 'no-auth';
        });

    }
}
