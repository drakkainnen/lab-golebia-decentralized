import { CollectionController, ICollection } from '../collection/collections.controller';
import { BirdDomainRestService } from '../common/bird-domain.rest-service';

export class CollectionsService {

    private collections: ICollection[];

    /* ngInject */
    constructor(private birdDomainRestService: BirdDomainRestService) {
    }

    updateCollections(ids?: number[]) {
        this.birdDomainRestService.getCollections()
            .then((data: ICollection[]) => {
                this.collections = data;
                if (ids) {
                    for (var c of data) {
                        if (ids.indexOf(c.id) > -1) {
                            c.selected = true;
                        }
                    }
                }
            });
    }

    getCollections() {
        return this.collections;
    }

    createCollection(newColl: ICollection): ng.IPromise<ICollection> {
        return this.birdDomainRestService.createCollection(newColl).
            then((data: ICollection) => {
                this.collections && this.collections.push(data);
                return data;
            });
    }

    deleteCollection(collectionToDelete: ICollection) {
        var index = this.collections.indexOf(collectionToDelete);
        if (index > -1) {
            this.collections.splice(index, 1);
            this.birdDomainRestService.deleteCollection(collectionToDelete.id);
        }
    }

    getSelectedCollections(): ICollection[] {
        return this.collections && this.collections.filter((collection: ICollection) => {
            return collection.selected;
        });
    }
}