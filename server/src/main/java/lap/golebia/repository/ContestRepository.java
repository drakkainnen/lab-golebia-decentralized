package lap.golebia.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import lap.golebia.document.Contest;

public interface ContestRepository extends MongoRepository<Contest, String>{
	public List<Contest> findAllByWinnersMark(String mark);

}
