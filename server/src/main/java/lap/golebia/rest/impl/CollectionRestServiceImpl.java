package lap.golebia.rest.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import lap.golebia.document.transport.BirdTo;
import lap.golebia.document.transport.CollectionTo;
import lap.golebia.rest.CollectionRestService;
import lap.golebia.service.CollectionService;

@RestController
public class CollectionRestServiceImpl implements CollectionRestService {

	@Autowired
	private CollectionService collectionService;

	@Override
	public List<CollectionTo> getBirdCollections() {
		return collectionService.getAllForLoggedUser();
	}

	@Override
	public CollectionTo createCollection(@RequestBody CollectionTo birdCollection) {
		return collectionService.createCollection(birdCollection);
	}

	@Override
	public CollectionTo getBirdCollectionById(@PathVariable("id") Long id) {
		return collectionService.getCollectionById(id);
	}

	@Override
	public void deleteBirdCollection(@PathVariable("id") Long id) {
		collectionService.deleteById(id);
	}

	@Override
	public Map<Long, Set<BirdTo>> getCollectionsWithBirds(@RequestBody ArrayList<Long> ids) {
		return collectionService.getCollectionsWithBirds(ids);
	}

}
