export interface ICredentials {
    username: string;
    password: string;
}

export class AuthorizationService {
    public authenticated: boolean = false;
    private loginPath: string = '/login';
    private logoutPath: string = '/logout';
    private homePath: string = '/';

    /* @ngInject */
    constructor(private $http: ng.IHttpService, private $location: ng.ILocationService) {
    }

    authenticate(credentials: ICredentials, callback?: Function): void {
        if(this.logMockUser(credentials, callback)) {
            return;
        }

        var basicAuthHeaders = credentials && credentials.username && credentials.password ? {
            authorization: 'Basic ' + btoa(credentials.username + ':' + credentials.password)
        } : {};
        var reqConf: angular.IRequestShortcutConfig = {};
        reqConf.headers = basicAuthHeaders;

        this.getUser(reqConf, callback);
    }

    clear(): void {
        this.authenticated = false;
        this.$location.path(this.homePath);
        this.$http.post(this.logoutPath, {});
    }

    init(homePath: string, loginPath: string, logoutPath: string): void {
        this.homePath = homePath;
        this.loginPath = loginPath;
        this.logoutPath = logoutPath;
    }

    private logMockUser(credentials: ICredentials, callback?: Function) {
        if(credentials && credentials.username === "mock" && credentials.password === "mock") {
            this.authenticated = true;
            if(callback) {
                callback(this.authenticated);
            }
            return true;
        }
        return false;
    }

    private getDetails() {
        this.$http.get('http://localhost:3333/userdata').then((data:any) => {
            console.log(data);
        });
    }

    private getUser(reqConf:angular.IRequestShortcutConfig, callback?: Function) {
        this.$http.get('http://localhost:3333/user', reqConf)
            .then((data: any) => {
                this.authenticated = data.name ? true : false;
                this.$location.path(this.homePath);
                if (callback) {
                    callback(this.authenticated);
                }
                this.getDetails();
            })
            .catch(() => {
                this.authenticated = false;
                if (callback) {
                    callback(this.authenticated);
                }
            });
    }
}
