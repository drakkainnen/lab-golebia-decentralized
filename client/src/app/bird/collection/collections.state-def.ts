import { CollectionsService } from './../common/collections.service';
import { ICollection } from './collections.controller';
import './collections.html';

export function addCollectionsState($stateProvider: angular.ui.IStateProvider) {
    var collectionsFn: Function = function (collectionsService: CollectionsService) {
        collectionsService.updateCollections();
    };

    $stateProvider.state('app.birds.collections', {
        url: '/collections',
        templateUrl: 'app/bird/collection/collections.html',
        controller: 'CollectionsCtrl',
        controllerAs: '$ctrl',
        resolve: {
            collections: collectionsFn
        }
    });
}
