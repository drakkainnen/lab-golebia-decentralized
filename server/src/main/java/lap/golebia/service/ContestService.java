package lap.golebia.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lap.golebia.document.Contest;
import lap.golebia.document.Winner;
import lap.golebia.document.transport.ContestTo;
import lap.golebia.repository.ContestRepository;
import ma.glasnost.orika.MapperFacade;

@Service
public class ContestService {
	
	private final ContestRepository contestRepository;
	private final MapperFacade mapperFacade;
	
	@Autowired
	public ContestService(ContestRepository contestRepository, MapperFacade mapperFacade) {
		this.contestRepository = contestRepository;
		this.mapperFacade = mapperFacade;
	}
	
	public List<ContestTo> getContestByMark(String mark) {
		return mapperFacade.mapAsList(contestRepository.findAllByWinnersMark(mark), ContestTo.class);
	}

}
