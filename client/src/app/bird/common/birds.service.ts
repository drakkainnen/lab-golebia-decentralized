import { ICollection } from '../collection/collections.controller';
import { Param } from 'angular-ui-router/release/ng1';
import { BirdDomainRestService } from './bird-domain.rest-service';
import { IBird } from '../item/item-bird.controller';
export class BirdsService {
    private bird2Collection: Map<number, IBird[]>;
    private birds: IBird[];

    constructor(private birdDomainRestService: BirdDomainRestService) {
    }

    updateBird2CollectionMap(collectionsIds: number[]) {
        this.birdDomainRestService.getBirdsToCollection(collectionsIds)
            .then((data) => {
                this.bird2Collection = new Map<number, IBird[]>();
                for (var property in data) {
                    if (data.hasOwnProperty(property)) {
                        this.bird2Collection.set(Number(property), data[property]);
                    }
                }
            });
    }

    getBirdsForCollection(collectionId: number): IBird[] {
        return this.bird2Collection && this.bird2Collection.get(collectionId);
    }

    getBirds() {
        return this.birds;
    }

    createBird(bird: IBird): angular.IPromise<IBird> {
        return this.birdDomainRestService.createBird(bird)
            .then((bird) => {
                this.birds && this.birds.push(bird);
                return bird;
            });
    }

    deleteBird(bird: IBird)  {
        var index = this.birds && this.birds.indexOf(bird);
        if (index > -1) {
            this.birdDomainRestService.deleteBird(bird.id)
                .then((bird: IBird) => {
                    this.birds.splice(index, 1);
                });
        }
    }

    addCollectionForBirds(birds: IBird[], collection: ICollection): ng.IPromise<IBird> {
        var birdIds = birds.map(function(v) {
            return v.id;
        });
        return this.birdDomainRestService.addAllBirdsToCollection(birdIds, collection.id);
    }

    fetchAllBirds() {
        this.birdDomainRestService.getAllBirds().then((birds: IBird[]) => {
            this.birds = birds;
        });
    }

    updateBird(bird: IBird) {
        this.birdDomainRestService.updateBird(bird);
    }

    updateAllAtOnce() {
        var reversed = this.reverseConnections();
        console.log(reversed);
    }

    private reverseConnections(): Map<number, IBird> {
        var result = new Map<number, IBird>();
        this.bird2Collection.forEach((birds: IBird[], key: number) => {
            birds.forEach((bird: IBird) => {
                var proc = result.get(bird.id);
                if (!proc) {
                    result.set(bird.id, bird);
                    bird.collections = [];
                    proc = bird;
                }
                proc.collections.push(key);
            });
        });
        return result;
    }

}