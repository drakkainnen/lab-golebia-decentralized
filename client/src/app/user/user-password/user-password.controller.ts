export class UserPasswordController {

    public showDialog: boolean;

    private newPassword: string;
    private repeatNewPassword: string;
    private oldPassword: string;

    toggleDialog() {
        this.showDialog = !this.showDialog;
        this.resetData();
    }

    changePassword() {
        this.toggleDialog();
        this.resetData();
    }

    private resetData() {
        this.oldPassword = '';
        this.repeatNewPassword = '';
        this.newPassword = '';
    }
}
