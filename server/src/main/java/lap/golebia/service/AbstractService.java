package lap.golebia.service;

import static java.util.Optional.ofNullable;

import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;

import lap.golebia.security.SecUser;
import ma.glasnost.orika.MapperFacade;

public abstract class AbstractService {
	
	@Autowired
	protected MapperFacade mapper;
	
	protected String getUsername() {
		return SecurityContextHolder.getContext().getAuthentication().getName();
	}
	
	protected Optional<SecUser> getPrincipal() {
		return ofNullable((SecUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
	}
	
	protected Long getUserId() {
		SecUser userDetails = (SecUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return Objects.nonNull(userDetails) ? userDetails.getId() : -1L;
	}
	
}
