import { AuthorizationService, ICredentials } from '../../auth/authorization.service';
export class LoginController {

    public loginError: string;
    private credentials: ICredentials;

    constructor(private authorizationService: AuthorizationService,
        private $state: angular.ui.IStateService) {
    }

    loginCallback: Function = (result: boolean) => {
        this.loginError = result ? '' : 'Bad credentials!';
        this.authorizationService.authenticated = true;
    }

    performLogin(): void {
        this.authorizationService.authenticate(this.credentials, this.loginCallback);
    }

    performLogout(): void {
        this.authorizationService.clear();
    }

    getButtonValue(): string {
        return this.$state.current.name === 'app.login' ? 'Login' : 'Logout';
    }

}
