package lap.golebia.document;

import lap.golebia.document.common.BirdColor;

public interface Bird {

	Long getId();

	void setId(Long id);

	String getRing();

	void setRing(String ring);

	BirdColor getColor();

	void setColor(BirdColor color);

	Long getUserId();

	void setUserId(Long userId);

	Boolean getGender();

	void setGender(Boolean gender);

}