import 'angular';
import { AuthorizationService } from './authorization.service';

angular.module('auth', [])
    .service('authorizationService', AuthorizationService);

export default angular.module('auth').name;
