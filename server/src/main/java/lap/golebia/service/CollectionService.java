package lap.golebia.service;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lap.golebia.document.dao.CollectionEntity;
import lap.golebia.document.transport.BirdTo;
import lap.golebia.document.transport.CollectionTo;
import lap.golebia.exceptions.UnauthorizedAccessException;
import lap.golebia.repository.CollectionRepository;

@Service
public class CollectionService extends AbstractService {

	@Autowired
	private CollectionRepository birdCollectionRepository;

	public List<CollectionTo> getAllForLoggedUser() {
		Set<CollectionEntity> collections = birdCollectionRepository.findByUser(this.getUserId());
		return this.mapper.mapAsList(collections, CollectionTo.class);
	}

	public CollectionTo createCollection(CollectionTo birdCollection) {
		CollectionEntity collection = this.mapper.map(birdCollection, CollectionEntity.class);
		collection.setUserId(this.getUserId());
		return this.mapper.map(birdCollectionRepository.save(collection), CollectionTo.class);
	}

	public CollectionTo getCollectionById(Long collectionId) {
		CollectionEntity result = birdCollectionRepository.findOne(collectionId);
		if (result.getUserId().equals(this.getUserId())) {
			return this.mapper.map(result, CollectionTo.class);
		}
		return null;
	}

	public void deleteById(Long id) {
		CollectionEntity result = birdCollectionRepository.findOne(id);
		if (Objects.nonNull(result) && this.getUserId().equals(result.getUserId())) {
			birdCollectionRepository.delete(result);
		} else {
			throw new UnauthorizedAccessException(
					String.format("collection with id %s does not exist or belongs to user different than %s", id,
							this.getUsername()));
		}
	}

	public List<Long> getAllCollectionsIds() {
		return birdCollectionRepository.findByUser(this.getUserId()).stream().map(CollectionEntity::getId)
				.collect(Collectors.toList());
	}
	
	public Map<Long, Set<BirdTo>> getCollectionsWithBirds(List<Long> ids) {
		if(!ids.isEmpty()) {
			Set<CollectionEntity> collectionWithBirds = birdCollectionRepository.findByIdsAndUserFetchBirds(getUserId(), ids);
			Map<Long, Set<BirdTo>> result = collectionWithBirds.stream().collect(Collectors.toMap(k -> k.getId(), l -> this.mapper.mapAsSet(l.getBirds(), BirdTo.class)));
			return result;
		}
		return null;
	}

}
