package lap.golebia.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lap.golebia.document.transport.ContestTo;
import lap.golebia.service.ContestService;

@RestController
public class ContestRestService {
	
	private final ContestService contestService;
	
	@Autowired
	public ContestRestService(ContestService contestService) {
		this.contestService = contestService;
	}
	
	@RequestMapping("/service/winner/{mark}")
	public List<ContestTo> getContestForBirdMark(@PathVariable String mark) {
		return contestService.getContestByMark(mark);
	}
	
}
