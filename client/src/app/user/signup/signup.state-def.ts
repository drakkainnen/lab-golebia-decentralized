import { SignupController } from './signup.controller';
import './signup.html';
export function addSignUpState($stateProvider: angular.ui.IStateProvider) {
    $stateProvider.state('app.signup', {
        url: 'signup',
        templateUrl: 'app/user/signup/signup.html',
        controller: SignupController,
        controllerAs: '$ctrl'
    });
}
