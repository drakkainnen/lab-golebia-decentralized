import { CollectionSetterController } from './collection-setter.controller';

export class CollectionSetterComponent implements angular.IComponentOptions {
    public controller: any;
    public controllerAs: string;
    public bindings: any; 
    public templateUrl: any;
    
    constructor() {
        this.controller = CollectionSetterController;
        this.controllerAs = '$ctrl';
        this.templateUrl = 'app/bird/birds/collection-setter/collection-setter.html';
        this.bindings = {
            birds: '='
        }
    }
}