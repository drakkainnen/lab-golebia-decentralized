package lap.golebia.rest.impl;

import java.security.Principal;
import java.util.Map;
import java.util.Objects;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import lap.golebia.document.transport.UserTo;
import lap.golebia.rest.UserRestService;
import lap.golebia.service.UserService;

@RestController
public class UserRestServiceImpl implements UserRestService {

	@Autowired
	private UserService userService;

	@Override
	public Principal user(Principal principal) {
		return principal;
	}

	@Override
	public UserTo getUser() {
		return userService.getLoggedUserDetails();
	}

	@Override
	public UserTo postUser(@RequestBody UserTo user, HttpServletResponse response) {
		user.setId(null);
		UserTo result = userService.updateUser(user);
		response.setStatus(result == null 
				? HttpServletResponse.SC_CONFLICT : HttpServletResponse.SC_OK);
		return result;
	}
	
	@Override
	public void postPassword(@RequestBody Map<String, String> values, Principal principal, HttpServletResponse response) {
		String oldPassword = values.get("oldPassword");
		String newPassword = values.get("newPassword");
		String newPasswordConfirm = values.get("newPasswordConfirm");
		if(Objects.equals(newPassword, newPasswordConfirm) && Objects.nonNull(newPassword)) {
			boolean result = userService.updatePassword(oldPassword, newPassword);
			response.setStatus(result ? HttpServletResponse.SC_OK : HttpServletResponse.SC_CONFLICT);
		} else {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}
	}
}
