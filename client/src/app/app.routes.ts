export default routesConfig;

/** @ngInject */
function routesConfig($stateProvider: angular.ui.IStateProvider,
  $urlRouterProvider: angular.ui.IUrlRouterProvider,
  $locationProvider: angular.ILocationProvider,
  $httpProvider: angular.IHttpProvider) {
  $locationProvider.html5Mode(true).hashPrefix('!');
  $urlRouterProvider.otherwise('/');

  $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
  $httpProvider.defaults.withCredentials = true;
  // $httpProvider.defaults.headers.common[""]

  $stateProvider
    .state('app', {
      url: '/',
      component: 'app'
    });
}

/** @ngInject */
function XSRFInterceptor($cookies, $log): ng.IHttpInterceptor {

  var XSRFInterceptor: ng.IHttpInterceptor = {};
  XSRFInterceptor.request = (config) => {
    var token = $cookies.get('XSRF-TOKEN');
    if (token) {
      config.headers['X-XSRF-TOKEN'] = token;
      $log.info("X-XSRF-TOKEN: " + token);
    }
    return config;
  };

  return XSRFInterceptor;
}