import { userpasswordComponent } from './user-password/user-password.component';
import { userRoutesConfig } from './user.routes';

angular.module('user', [])
    .config(userRoutesConfig)
    .component('changePassword', userpasswordComponent);

export default angular.module('user').name;
