import { IBird } from '../../item/item-bird.controller';
import { BirdsService } from '../../common/birds.service';

export class EditBirdController implements angular.IComponentController {

    private bird: IBird;
    private isParsable: boolean = true;

    public department: string;
    public year: string;
    public number: string;
    public gender: number;

    constructor(private birdsService: BirdsService, $scope: angular.IScope) {
        $scope.$watch(() => { return this.bird }, (newValue, oldValue) => {
            this.selectionChangeListener(newValue, oldValue);
        });

        $scope.$watch(() => {
            return this.createRing();
        }, (newValue, oldValue) => {
            if (this.bird) {
                this.bird.ring = this.createRing();
            }
        });
    }

    selectionChangeListener(newValue: any, oldValue: any) {
        if (newValue) {
            this.parseRing();
        } else {
            this.isParsable = true;
        }
    }

    parseRing() {
        var reg: RegExp = /^PL-(\d+)-(\d+)-(\d+)-(\d)$/;
        var regexGroups = { department: 1, year: 2, number: 3, gender: 4 };
        var res = reg.exec(this.bird.ring);
        console.log(res);
        if (res) {
            this.isParsable = true;
            this.department = res[regexGroups.department];
            this.year = res[regexGroups.year];
            this.number = res[regexGroups.number];
            this.gender = +res[regexGroups.gender];
        } else {
            this.isParsable = false;
        }
        console.log(this.isParsable);
    }

    createRing() {
        return ['PL', this.department.trim(), this.year.trim(), this.number.trim(), this.gender].join('-');
    }

    save() {
        if (this.isBirdSelected()) {
            this.birdsService.updateBird(this.bird);
        }
        else {
            this.bird.ring = this.createRing();
            this.birdsService.createBird(this.bird);
            this.clear();
        }
    }

    private clear() {
        this.bird = null;
    }

    private isBirdSelected() {
        return this.bird && this.bird.id;
    }
}
