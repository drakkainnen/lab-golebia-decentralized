import { NavigationController } from './navigation.controller';
import './navigation.html';

export const navigationComponent: angular.IComponentOptions = {
  templateUrl: 'app/navigation/navigation.html',
  controller: NavigationController
};
