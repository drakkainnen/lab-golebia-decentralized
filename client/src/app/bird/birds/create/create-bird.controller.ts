import { isNumber } from 'angular-ui-router/commonjs/ng1';
import { IBird } from '../../item/item-bird.controller';
import { BirdsService } from '../../common/birds.service';

export class CreateBirdController implements angular.IComponentController {

    private bird: IBird = <IBird>{};
    private isParsable: boolean = true;
    private isMore: boolean;

    public department: string = "";
    public year: string = "";
    public number: string = "";
    public gender: number;

    constructor(private birdsService: BirdsService, $scope: angular.IScope) {
        $scope.$watch(() => {
            return this.createRing();
        }, (newValue, oldValue) => {
            this.bird.ring = this.createRing();
        });
    }

    parseRing() {
        var reg: RegExp = /^PL-(\d+)-(\d+)-(\d+)-(\d)$/;
        var regexGroups = { department: 1, year: 2, number: 3, gender: 4 };
        var res = reg.exec(this.bird.ring);
        console.log(res);
        if (res) {
            this.department = res[regexGroups.department];
            this.year = res[regexGroups.year];
            this.number = res[regexGroups.number];
            this.gender = +res[regexGroups.gender];
        } else {
            this.isParsable = false;
        }
        console.log(this.isParsable);
    }

    createRing() {
        return ['PL', this.department && this.department.trim(),
            this.year && this.year.trim(),
            this.number && this.number.trim(),
            this.gender].join('-');
    }

    save() {
        this.birdsService.createBird(this.bird).then((bird:IBird) => {
            this.clear();
        });
    }

    toggleMore() {
        this.isMore = !this.isMore;
    }

    private clear() {
        this.bird = <IBird>{};
        this.gender = null;
        this.number = null;
        document.getElementById('numberField').focus();
    }


    private isBirdSelected() {
        return this.bird && this.bird.id;
    }
}
