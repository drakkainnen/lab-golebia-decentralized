package lap.golebia.document.dao;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import lap.golebia.document.Bird;
import lap.golebia.document.common.BirdColor;

@Entity
@Table(name = "bird")
public class BirdEntity implements Bird {

	@Id
	@GeneratedValue
	private Long id;

	@Column(unique = true, nullable = false)
	private String ring;

	private BirdColor color;

	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "bird_collection", joinColumns = @JoinColumn(name = "bird_id", referencedColumnName = "id", updatable = true, insertable = true), inverseJoinColumns = @JoinColumn(name = "collection_id", referencedColumnName = "id", updatable = true, insertable = true))
	private Set<CollectionEntity> collections;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id")
	private UserEntity user;

	@Column(name = "user_id", updatable = false, insertable = false)
	private Long userId;

	@Column(columnDefinition = "TINYINT")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean gender;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String getRing() {
		return ring;
	}

	@Override
	public void setRing(String ring) {
		this.ring = ring;
	}

	@Override
	public BirdColor getColor() {
		return color;
	}

	@Override
	public void setColor(BirdColor color) {
		this.color = color;
	}

	public Set<CollectionEntity> getCollection() {
		return this.collections;
	}

	public void setCollection(Set<CollectionEntity> collection) {
		this.collections = collection;
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	@Override
	public Long getUserId() {
		return this.userId;
	}

	@Override
	public void setUserId(Long userId) {
		UserEntity user = new UserEntity();
		user.setId(userId);
		this.user = user;
		this.userId = userId;
	}

	@Override
	public Boolean getGender() {
		return gender;
	}

	@Override
	public void setGender(Boolean gender) {
		this.gender = gender;
	}
}
