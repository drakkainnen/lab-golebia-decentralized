package lap.golebia.service;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import lap.golebia.document.dao.BirdEntity;
import lap.golebia.document.dao.CollectionEntity;
import lap.golebia.document.dao.UserEntity;
import lap.golebia.repository.BirdRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class BirdServiceTest {

	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	private BirdRepository birdRepository;

	@Autowired
	private BirdService birdService;

	@Test
	public void testExample() {
		UserEntity user = new UserEntity();
		user.setFirstname("a");
		user.setLastname("a");
		user.setUsername("a");
		user.setEmail("a@a.com");
		user.setPassword("a");

		BirdEntity bird = new BirdEntity();
		bird.setRing("123");

		CollectionEntity collectionEntity = new CollectionEntity();
		collectionEntity.setName("ddd");

		entityManager.persist(user);
		entityManager.persist(bird);
		entityManager.persist(collectionEntity);

		ArrayList<Long> arrayList = new ArrayList<>();
		arrayList.add(bird.getId());
		birdService.addBirdsToCollection(arrayList, collectionEntity.getId());
	}
}
