export interface IPerson {
    name: string;
    lastName: string;
    password: string;
    passwordAnother: string;
    username: string;
    email: string;
}

export class SignupController {
    private person: IPerson;
    private success: boolean;
    // private showError: boolean;

    constructor(private $http: ng.IHttpService) {
    }

    doSignup() {
        this.$http.post('user', this.person)
            .then((response: any) => {
                this.success = true;
            })
            .catch((data: any) => {
                this.success = false;
            });
    }
    checkPassword() {
        if (this.person && this.person.password) {
            return this.person.password === this.person.passwordAnother;
        }
    }
}
