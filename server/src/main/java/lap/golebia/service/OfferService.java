package lap.golebia.service;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lap.golebia.document.Offer;
import lap.golebia.document.Transaction;
import lap.golebia.document.dao.BirdEntity;
import lap.golebia.document.transport.CollectionTo;
import lap.golebia.document.transport.BirdTo;
import lap.golebia.document.transport.OfferTo;
import lap.golebia.exceptions.ObjectAlreadyExistsException;
import lap.golebia.exceptions.UnauthorizedAccessException;
import lap.golebia.repository.OfferRepository;
import lap.golebia.repository.TransactionRepository;
import ma.glasnost.orika.MapperFacade;

@Service
public class OfferService {

//	private static final String DEFAULT_BOUGHT_COLLECTION_NAME = "bought";
//	private final OfferRepository offerRepository;
//	private final MapperFacade mapperFacade;
//	private final BirdService birdService;
//	private final CollectionService collectionService;
//	private final TransactionRepository transactionRepository;
//
//	@Autowired
//	public OfferService(MapperFacade mapperFacade, OfferRepository offerRepository, CollectionService collectionService,
//			BirdService birdService, TransactionRepository transactionRepository) {
//		this.offerRepository = offerRepository;
//		this.mapperFacade = mapperFacade;
//		this.birdService = birdService;
//		this.collectionService = collectionService;
//		this.transactionRepository = transactionRepository;
//	}
//
//	public List<OfferTo> getAllOfferts() {
//		return mapperFacade.mapAsList(offerRepository.findAll(), OfferTo.class);
//	}
//
//	public OfferTo insertOffer(OfferTo offerTo) {
//		Offer offert = mapperFacade.map(offerTo, Offer.class);
//		return mapperFacade.map(offerRepository.insert(offert), OfferTo.class);
//	}
//
//	public OfferTo getOffertById(String id) {
//		return mapperFacade.map(offerRepository.findOne(id), OfferTo.class);
//	}
//
//	public void deleteOffert(String id, String username) {
//		Offer findOneByIdAndByUsername = offerRepository.findOneByIdAndUsername(id, username);
//		if (findOneByIdAndByUsername != null) {
//			offerRepository.delete(id);
//		} else {
//			throw new UnauthorizedAccessException(
//					String.format("offer %s belongs to other user than %s", id, username));
//		}
//	}
//
//	public void buy(String offerId, String name) {
//		OfferTo offer = getOffertById(offerId);
//		if (!Objects.equals(offer.getUsername(), name)) {
//			BirdTo bird = offer.getBird();
//			Transaction transaction = new Transaction();
//			transaction.setBird(mapperFacade.map(bird, Bird.class));
//			transaction.setBuyerName(name);
//			transaction.setSellerName(offer.getUsername());
//
//			Optional<BirdCollectionTo> collection = collectionService.getAllCollections(name).stream()
//					.filter(b -> Objects.equals(b.getCollectionName(), DEFAULT_BOUGHT_COLLECTION_NAME)).findFirst();
//			if (collection.isPresent()) {
//				bird.setCollection(Arrays.asList(collection.get()));
//			} else {
//				BirdCollectionTo bought = new BirdCollectionTo();
//				bought.setCollectionName(DEFAULT_BOUGHT_COLLECTION_NAME);
////				BirdCollectionTo newCollection = collectionService.addCollection(bought, name);
////				bird.setCollection(Arrays.asList(newCollection));
//			}
//			birdService.updateBirdOwner(bird, name);
//			transactionRepository.save(transaction);
//			offerRepository.delete(offer.getId());
//		}
//	}
//
//	public List<OfferTo> getAllOfferts(String name) {
//		return mapperFacade.mapAsList(offerRepository.findAllByUsername(name), OfferTo.class);
//	}
//
//	public void checkIfBirdOnSale(String id) {
//		Offer offer = offerRepository.findOneByBirdId(id);
//		if (offer != null) {
//			throw new ObjectAlreadyExistsException(String.format("offer %s already exists", offer.getId()));
//		}
//	}
//
//	public void addComment(String id, String comment) {
//		Offer findOne = offerRepository.findOne(id);
//		if (findOne != null) {
//			if(findOne.getComments() == null) {
//				findOne.setComments(new LinkedList<>());
//			}
//			findOne.getComments().add(comment);
//			offerRepository.save(findOne);
//		}
//	}
//
}
