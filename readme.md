# Projekt: Lap golebia

## Ustawienie środowiska
1. zaciągamy repo
>		git clone https://drakkainnen@bitbucket.org/drakkainnen/lab-golebia-decentralized.git
1. otwieramy konsole w katalogu projektu
1. odpalamy skrypt gradlew.bat w kolejności:
>		gradlew.bat tasks
>		gradlew.bat nodeSetup
>		gradlew.bat npmSetup
>		gradlew.bat npmInstall
>		gradlew.bat installGulp

## Odpalanie servera
Żeby poprawnie skompilować i uruchomić server, trzeba:

1. zainstalować Java JDK w wersji 1.8

2. ustawić JAVA_HOME

3. uruchomić gradlowy taskt, który skompiluje projekt i uruchomi server
>		gradlew.bat bootRun

## Odpalanie klienta
Odpalanie klienta
>		gradlew.bat gulp_serve

## Edycja projektu
Importujemy w dowolnym środowisku wspierającym gradle. Np. Eclipse z wtyczką do gradle.
