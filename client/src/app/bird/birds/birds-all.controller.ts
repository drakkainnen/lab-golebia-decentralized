import { CollectionsService } from '../common/collections.service';
import { ICollection } from '../collection/collections.controller';
import { IBird } from '../item/item-bird.controller';
import { BirdsService } from '../common/birds.service';
export class BirdsAllController {

    public editedBird: IBird;
    public selected: IBird[];

    /* @ng-inject */
    constructor(private birdsService: BirdsService,
        private collectionsService: CollectionsService) {
        this.selected = [];
    }

    getBirds(): IBird[] {
        return this.birdsService.getBirds();
    }

    delete(bird: IBird) {
        this.birdsService.deleteBird(bird);
    }

    edit(bird: IBird) {
        if (this.editedBird === bird) {
            this.editedBird = null;
        }
        else {
            this.editedBird = bird;
        }
    }

    select(bird: IBird) {
        this.toggleSelection(bird);
        if (bird.selected) {
            this.selected.push(bird);
        } else {
            var idx = this.selected.indexOf(bird);
            if (idx > -1) {
                this.selected.splice(idx, 1);
            }
        }
    }

    connectSelected(item: ICollection) {
        this.collectionsService.createCollection(item).
            then((collection: ICollection) => {
                return this.birdsService.addCollectionForBirds(this.selected, collection)
            })
            .then(() => {
                console.log('OK');
            });
    }

    private toggleSelection(bird: IBird) {
        if (bird) {
            bird.selected = !bird.selected;
        }
    }
}
