package lap.golebia.rest;

import java.security.Principal;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import lap.golebia.document.transport.UserTo;

public interface UserRestService {

	@GetMapping("/user")
	Principal user(Principal principal);

	@GetMapping("/userdata")
	UserTo getUser();

	@PostMapping("/user")
	UserTo postUser(UserTo user, HttpServletResponse response);

	@PostMapping(value = "/service/password")
	void postPassword(Map<String, String> values, Principal principal, HttpServletResponse response);

}