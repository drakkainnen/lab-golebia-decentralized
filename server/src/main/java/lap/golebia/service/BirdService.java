package lap.golebia.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lap.golebia.document.dao.BirdEntity;
import lap.golebia.document.dao.CollectionEntity;
import lap.golebia.document.transport.BirdTo;
import lap.golebia.document.transport.CollectionTo;
import lap.golebia.repository.BirdRepository;
import lap.golebia.repository.CollectionRepository;

@Service
public class BirdService extends AbstractService {

	@Autowired
	private BirdRepository birdRepository;

	@Autowired
	private CollectionRepository collectionRepository;

	public BirdTo getById(Long id) {
		return this.mapper.map(birdRepository.findOne(id), BirdTo.class);
	}

	public List<BirdTo> getAllBirdsForCurrentUser() {

		List<BirdEntity> birds = birdRepository.findAllByUserId(this.getUserId());
		return this.mapper.mapAsList(birds, BirdTo.class);
	}

	@Transactional
	public BirdTo create(BirdTo birdTo) {
		// TODO: might throw Unique Constraint exception
		// BirdTo birdWithMark = birdRepository.findOneByMark(birdTo.getMark());
		// throwIfBirdExists(birdWithMark);
		// throwIfNotContainsAllCollections(birdTo, username);

		BirdEntity BirdEntity = this.mapper.map(birdTo, BirdEntity.class);
		BirdEntity.setUserId(this.getUserId());

		List<Long> ids = birdCollectionIds(birdTo);
		if (!ids.isEmpty()) {
			BirdEntity.setCollection(collectionRepository.findByIdsAndUser(this.getUserId(), ids));
		}
		return this.mapper.map(birdRepository.save(BirdEntity), BirdTo.class);

	}

	//
	// private void throwIfNotContainsAllCollections(BirdTo birdTo, String
	// username) {
	//// List<String> birdCollectionIds = birdCollectionIds(birdTo);
	//// List<String> allCollectionsIds =
	// birdCollectionService.getAllCollectionsIds(username);
	//// if (!allCollectionsIds.containsAll(birdCollectionIds) ||
	// birdCollectionIds.isEmpty()
	//// || allCollectionsIds.isEmpty()) {
	//// throw new UnauthorizedAccessException(
	//// String.format("one or more collections not found for user %s",
	// username));
	//// }
	// }
	//
	// private void throwIfBirdExists(BirdEntity birdWithMark) {
	// if (birdWithMark != null) {
	// throw new ObjectAlreadyExistsException(
	// String.format("BirdEntity with mark %s already exists",
	// birdWithMark.getMark()));
	// }
	// }
	//
	@Transactional
	public BirdTo update(BirdTo birdTo) {
		// TODO: might not work
		BirdEntity result = birdRepository.findByIdAndUserId(birdTo.getId(), this.getUserId());
		if (result != null) {
			this.mapper.map(birdTo, result);
			return this.mapper.map(result, BirdTo.class);
		}
		return null;
	}

	//
	// private void checkUser(String username, BirdEntity BirdEntity) {
	// if (Objects.equals(BirdEntity.getUsername(), username)) {
	// throw new UnauthorizedAccessException("BirdEntity update failed beacuse u
	// want acces not your BirdEntity");
	// }
	// }
	//
	// private void checkForCollision(BirdTo birdTo, List<BirdEntity> results) {
	// if (results.size() > 2) {
	// throw new ObjectAlreadyExistsException(String.format("BirdEntity with
	// mark %s exists", birdTo.getMark()));
	// }
	// }
	//
	// private BirdTo updateIfSameBirdEntity(BirdTo birdTo, BirdEntity
	// BirdEntity) {
	// if (!Objects.equals(BirdEntity.getId(), birdTo.getId())) {
	// throwIfNotContainsAllCollections(birdTo, BirdEntity.getName());
	// BirdEntity updatedBirdEntity = this.mapper.map(birdTo, BirdEntity.class);
	// return this.mapper.map(birdRepository.save(updatedBirdEntity),
	// BirdTo.class);
	// }
	// throw new ObjectAlreadyExistsException(String.format("BirdEntity with
	// given id %s already exists", birdTo.getId()));
	// }
	//
	public void deleteById(Long id) {
		Optional<BirdEntity> bird = Optional.ofNullable(birdRepository.findByIdAndUserId(id, this.getUserId()));
		bird.ifPresent(b -> this.birdRepository.delete(b));
	}

	public List<BirdTo> getByCollectionId(Long id) {
		List<BirdEntity> birds = birdRepository.findByCollectionId(id);
		return this.mapper.mapAsList(birds, BirdTo.class);
	}

	public BirdTo getBird(Long id) {
		BirdEntity oneUser = birdRepository.findOne(id);
		return this.mapper.map(oneUser, BirdTo.class);
	}

	private List<Long> birdCollectionIds(BirdTo birdTo) {
		if (Objects.nonNull(birdTo.getCollections()) && !birdTo.getCollections()
				.isEmpty()) {
			return birdTo.getCollections()
					.stream()
					.map(CollectionTo::getId)
					.collect(Collectors.toList());
		}
		return Collections.emptyList();
	}

	@Transactional
	public void addBirdsToCollection(ArrayList<Long> birdIds, Long collectionId) {
		CollectionEntity collection = collectionRepository.findByIdAndUser(getUserId(), collectionId);
		if (Objects.nonNull(collection)) {
			Set<BirdEntity> birds = birdRepository.findAllByIds(birdIds, getUserId());
			collection.setBirds(birds);
			birds.stream()
					.forEach(b -> setCollection(b, collection));
		}
	}

	private void setCollection(BirdEntity bird, CollectionEntity collection) {
		bird.getCollection()
				.add(collection);
	}
}
