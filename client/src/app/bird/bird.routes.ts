import { addBirdsAllState } from './birds/birds-all.state-def';
import { addViewsCollectionState } from './collection/views/views-collection.state-def';
import { addCollectionsState } from './collection/collections.state-def';
import { addBirdsState } from './birds.state-def';

export function birdRoutesConfig($stateProvider: angular.ui.IStateProvider) {
    addBirdsState($stateProvider);
    addBirdsAllState($stateProvider);
    addCollectionsState($stateProvider);
    addViewsCollectionState($stateProvider);
}
