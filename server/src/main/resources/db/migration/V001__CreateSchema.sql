
create table user (
	id bigint not null primary key auto_increment,
	username varchar(200) not null,
	password varchar(200) not null,
	email varchar(200) not null,
	firstname varchar(200) not null,
	lastname varchar(200) not null,
	department varchar(200),
	latitude double,
	longtitude double
);

create table collection (
	id bigint not null primary key auto_increment,
	name varchar(40) not null,
	description varchar(2000),
	user_id bigint not null,

	constraint fk_collection_user
		foreign key (user_id) references user(id) on delete cascade
);

create table bird (
	id bigint not null primary key auto_increment,
	ring varchar(40) not null unique,
	color int,
	gender tinyint(1),
	user_id bigint not null,
	
	constraint fk_bird_user
		foreign key (user_id) references user(id) on delete cascade
);

create table bird_collection (
	bird_id bigint not null,
	collection_id bigint not null,
	
	primary key (bird_id, collection_id),
	constraint fk_bird_collection
		foreign key (bird_id) references bird(id) on delete cascade,
	constraint fk_collection_bird
		foreign key (collection_id) references collection(id) on delete cascade
);