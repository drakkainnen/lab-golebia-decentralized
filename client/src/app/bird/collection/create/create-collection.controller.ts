import { ICollection } from '../collections.controller';
export class CreateCollectionController {
    public showDialog: boolean;
    private collection: ICollection;
    private onCreate: Function;

    toggleDialog() {
        this.showDialog = !this.showDialog;
    }

    saveCollection() {
        var newCollection: ICollection = {
            name: this.collection.name,
            description: this.collection.description
        };
        this.onCreate({ item: newCollection });
    }
}
