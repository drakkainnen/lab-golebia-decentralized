package lap.golebia.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;

@Configuration
@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private UserDetailsService userDetailsService;
	
	private PasswordEncoder encoder = new BCryptPasswordEncoder();

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		//@formatter:off
		http
			.httpBasic()
		.and()
               .authorizeRequests()
               	.antMatchers("/index.html", "/", "/vendor-*.js", "/app-*.js", "/index-*.css", "/app/user/user-password/user-password.html", "/app/navigation/navigation.html").permitAll()
               	.antMatchers(HttpMethod.POST, "/user").permitAll()
               	.antMatchers(HttpMethod.OPTIONS).permitAll()
				.anyRequest().authenticated()
		.and()
		.csrf()
				.disable();
//				.csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse());
//		@formatter:on
	}
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		//@formatter:off	
		auth
			.userDetailsService(userDetailsService)
//			.passwordEncoder(encoder)
		.and()
			.inMemoryAuthentication()
//			.passwordEncoder(encoder)
				.withUser("bb")
//					.password("$2a$10$V5SiTMvOT2Rk1tasvs/vFuKZt.RWFwn3c9KS3CiIHPlVCGaXtFWqC")
					.password("bb")
					.roles("USER");
		//@formatter:on	
	}
}
