package lap.golebia.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import lap.golebia.document.dao.UserEntity;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {

	@Query("select user from UserEntity user " + "where user.username like :username")
	public UserEntity findByUsername(@Param("username") String username);

	@Query("select user from UserEntity user " + "where user.username like :username " + "or user.email like :email")
	public Set<UserEntity> findByUsernameAndEmail(@Param("username") String username, @Param("email") String email);

}
