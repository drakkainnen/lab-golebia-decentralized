package lap.golebia.rest;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import lap.golebia.document.transport.OfferTo;
import lap.golebia.service.BirdService;
import lap.golebia.service.OfferService;

@RestController
public class OffertRestService {
//
//	private final OfferService offerService;
//
//	private final BirdService birdService;
//
//	@Autowired
//	public OffertRestService(OfferService offerService, BirdService birdService) {
//		this.offerService = offerService;
//		this.birdService = birdService;
//	}
//
//	@RequestMapping(value = "/service/offerts")
//	public List<OfferTo> getOfferts() {
//		return offerService.getAllOfferts();
//	}
//	
//	@RequestMapping(value = "/service/my/offerts")
//	public List<OfferTo> getMyOfferts(Principal principal) {
//		return offerService.getAllOfferts(principal.getName());
//	}
//
//	@RequestMapping(value = "/service/offerts", method = RequestMethod.POST)
//	public OfferTo postOfferts(@RequestBody OfferTo offer, Principal principal, BindingResult bindings) {
//		birdService.getBird(offer.getBird().getId(), principal.getName());
//		offerService.checkIfBirdOnSale(offer.getBird().getId());
//		offer.setUsername(principal.getName());
//		OfferTo insertOffer = offerService.insertOffer(offer);
//		return insertOffer;
//	}
//	
//	@RequestMapping(value = "/service/offerts/comment/{id}", method = RequestMethod.POST)
//	public void postComment(@PathVariable("id") String id, @RequestBody String comment) {
//		offerService.addComment(id, comment);
//	}
//
//	@RequestMapping(value = "/service/offerts/{id}", method = RequestMethod.GET)
//	public OfferTo getOffertById(@PathVariable("id") String id) {
//		 OfferTo offertById = offerService.getOffertById(id);
//		 return offertById;
//	}
//
//	@RequestMapping(value = "/service/offerts/{id}", method = RequestMethod.DELETE)
//	public void deleteOffert(@PathVariable("id") String id, Principal principal) {
//		offerService.deleteOffert(id, principal.getName());
//	}
//
//	@RequestMapping(value = "/service/buy", method = RequestMethod.POST)
//	public void postBuy(@RequestBody String offerId, Principal principal) {
//		offerService.buy(offerId, principal.getName());
//	}
//
}
