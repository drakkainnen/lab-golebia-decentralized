import { ItemBirdController } from './item-bird.controller';
import './item-bird.html';
export const itemBirdComponent: angular.IComponentOptions = {
    templateUrl: 'app/bird/item/item-bird.html',
    controller: ItemBirdController,
    bindings: {
        // bird: '='
    }
};
