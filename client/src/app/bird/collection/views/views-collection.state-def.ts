import { CollectionsService } from '../../common/collections.service';
import { BirdsService } from '../../common/birds.service';
import { BirdDomainRestService } from '../../common/bird-domain.rest-service';
import './views-collection.html';
import { ViewsCollectionController } from './views-collection.controller';
export function addViewsCollectionState($stateProvider: angular.ui.IStateProvider) {

    var getCollectionsIdsFn: Function = function ($stateParams: any): number[] {
        var collectionsIdsAsString: string = $stateParams.collectionIds;
        if (collectionsIdsAsString) {
            return collectionsIdsAsString.split(',')
                .map(function (value: string) {
                    return Number(value);
                });
        }
    };

    var birdsToCollectionFn: Function = function (collectionsIds: number[], birdsService: BirdsService, collectionsService: CollectionsService) {
        birdsService.updateBird2CollectionMap(collectionsIds);
        var collections = collectionsService.getCollections()
        if(!collections) {
            collectionsService.updateCollections(collectionsIds);
        }
    }

    $stateProvider.state('app.birds.collections.collection', {
        url: '/{collectionIds}',
        templateUrl: 'app/bird/collection/views/views-collection.html',
        controller: ViewsCollectionController,
        controllerAs: '$ctrl',
        resolve: {
            collectionsIds: getCollectionsIdsFn,
            birdsToCollection: birdsToCollectionFn
        }
    });
}
