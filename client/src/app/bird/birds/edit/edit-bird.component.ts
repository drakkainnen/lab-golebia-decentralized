import { EditBirdController } from './edit-bird.controller';

export class EditBirdComponent implements angular.IComponentOptions {

    public controller: any;
    public controllerAs: string;
    public templateUrl: string;
    public bindings: { [binding: string]: string };

    constructor() {
        this.templateUrl = 'app/bird/birds/create/create-bird.html';
        this.controller = EditBirdController;
        this.controllerAs = '$ctrl';
        this.bindings = {
            bird: '<'
        }
    }
}
