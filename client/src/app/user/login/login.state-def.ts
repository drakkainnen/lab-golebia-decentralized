import { AuthorizationService } from '../../auth/authorization.service';
import { LoginController } from './login.controller';
export function addLoginState($stateProvider: angular.ui.IStateProvider) {
    $stateProvider.state('app.login', {
        url: 'login',
        templateUrl: 'app/user/login/login.html',
        controller: LoginController,
        controllerAs: '$ctrl'
    })
        .state('app.logout', {
            url: 'logout',
            onEnter: function (authorizationService: AuthorizationService) {
                authorizationService.clear();
            }
        });
}
