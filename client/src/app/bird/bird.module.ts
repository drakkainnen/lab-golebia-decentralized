import { CollectionSetterComponent } from './birds/collection-setter/collection-setter.component';
import { CreateBirdComponent } from './birds/create/create-bird.component';
import { BirdsService } from './common/birds.service';
import { CollectionsService } from './common/collections.service';
import { BirdDomainRestService } from './common/bird-domain.rest-service';
import 'angular-ui-router';
import { birdRoutesConfig } from './bird.routes';
import { CollectionController } from './collection/collections.controller';
import { createcollectionComponent } from './collection/create/create-collection.component';
import { itemBirdComponent } from './item/item-bird.component';
import * as angular from 'angular';
var dragula = require('angular-dragula');

angular.module('bird', [dragula(angular)])
    .config(birdRoutesConfig)
    .component('createCollection', createcollectionComponent)
    .component('createBird', new CreateBirdComponent())
    .component('birdItem', itemBirdComponent)
    .component('collectionSetter', new CollectionSetterComponent())

    .controller('CollectionsCtrl', CollectionController)

    .service('birdDomainRestService', BirdDomainRestService)
    .service('collectionsService', CollectionsService)
    .service('birdsService', BirdsService);


export default angular.module('bird').name;
