package lap.golebia.document.transport;

import java.util.Set;

import lap.golebia.document.User;

public class UserTo implements User {
	private Long id;
	private String username;
	private String password;
	private String firstname;
	private String lastname;
	private String department;
	private String email;
	private Set<CollectionTo> collection;
	private Double latitude;
	private Double longtitude;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String getUsername() {
		return username;
	}

	@Override
	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String getFirstname() {
		return firstname;
	}

	@Override
	public void setFirstname(String firstName) {
		this.firstname = firstName;
	}

	@Override
	public String getLastname() {
		return lastname;
	}

	@Override
	public void setLastname(String lastName) {
		this.lastname = lastName;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	@Override
	public String getEmail() {
		return email;
	}

	@Override
	public void setEmail(String email) {
		this.email = email;
	}

	public Set<CollectionTo> getCollection() {
		return collection;
	}

	public void setCollection(Set<CollectionTo> collection) {
		this.collection = collection;
	}

	@Override
	public Double getLatitude() {
		return this.latitude;
	}

	@Override
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	@Override
	public Double getLongtitude() {
		return this.longtitude;
	}

	@Override
	public void setLongtitude(Double longtitude) {
		this.longtitude = longtitude;
	}
}
