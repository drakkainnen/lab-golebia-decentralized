import { BirdsAllController } from './birds-all.controller';
import { BirdsService } from '../common/birds.service';

export function addBirdsAllState($stateProvider: angular.ui.IStateProvider) {
    var fetchBirdsFn:Function = function(birdsService: BirdsService) {
        birdsService.fetchAllBirds();
    };

    $stateProvider.state('app.birds.birds', {
        url: '/birds',
        controller: BirdsAllController,
        controllerAs: '$ctrl',
        templateUrl: 'app/bird/birds/birds-all.html',
        resolve: {
            fetchBirds: fetchBirdsFn
        }
    });
}
