package lap.golebia.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import lap.golebia.document.Transaction;

public interface TransactionRepository extends MongoRepository<Transaction, String>{

}
