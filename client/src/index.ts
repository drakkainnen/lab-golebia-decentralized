import '../node_modules/bootstrap/dist/css/bootstrap.css';
import '../node_modules/angular-dragula/dist/dragula.css';
import './index.less';
import 'angular-ui-router';
import { appComponent } from './app/app.component';
import routesConfig from './app/app.routes';
import XSRFInterceptor from './app/xsrf-interceptor.factory';
import authorization from './app/auth/authorization.module';
import bird from './app/bird/bird.module';
import { navigationComponent } from './app/navigation/navigation.component';
import user from './app/user/user.module';
import * as angular from 'angular';

/// <reference path="../typings/index.d.ts" />

export const app: string = 'app';

angular
  .module(app, ['ui.router', authorization, bird, user])
  .config(routesConfig)
  .constant('rootUrl', 'http://localhost:3333/')
  .component('app', appComponent)
  .component('navigation', navigationComponent);

  // .factory('XSRFInterceptor', XSRFInterceptor);
