package lap.golebia;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.web.WebAppConfiguration;

import lap.golebia.LapGolebiaApplication;

import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = LapGolebiaApplication.class)
@WebAppConfiguration
public class LapGolebiaApplicationTests {

	@Test
	public void contextLoads() {
	}

}
