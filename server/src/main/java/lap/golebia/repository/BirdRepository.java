package lap.golebia.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import lap.golebia.document.dao.BirdEntity;

@Repository
public interface BirdRepository extends JpaRepository<BirdEntity, Long> {

	@Query("select bird from BirdEntity bird "
			+ "inner join bird.collections collection "
			+ "where collection.id = :id")
	public List<BirdEntity> findByCollectionId(@Param("id") Long id);

	@Query("select bird from BirdEntity bird " + "where bird.ring like :ring")
	public BirdEntity findByRing(@Param("ring") String ring);

	@Query("select bird from BirdEntity bird " + "where bird.id = :id " + "and bird.user.id = :userId")
	public BirdEntity findByIdAndUserId(@Param("id") Long id, @Param("userId") Long userId);

	@Query("select bird from BirdEntity bird " + "where bird.user.id = :userId")
	public List<BirdEntity> findAllByUserId(@Param("userId") Long userId);

	@Query("select bird from BirdEntity bird "
			+ "inner join fetch bird.collections collection "
			+ "where collection.id in :collectionsIds and collection.user.id = :userId")
	public List<BirdEntity> findAllByCollectionIds(@Param("collectionsIds") List<Long> collectionId,
			@Param("userId") Long userId);

	@Query("select bird from BirdEntity bird "
			+ "inner join fetch bird.collections collection "
			+ "where bird.id in :birdIds "
			+ "and bird.user.id = :userId")
	public Set<BirdEntity> findAllByIds(@Param("birdIds") ArrayList<Long> birdIds, @Param("userId") Long userId);

}
