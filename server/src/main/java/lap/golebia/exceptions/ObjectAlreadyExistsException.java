package lap.golebia.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@SuppressWarnings("serial")
@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "object already exists with given data")
public class ObjectAlreadyExistsException extends RuntimeException {
	public ObjectAlreadyExistsException(String message) {
		super(message);
	}
}
