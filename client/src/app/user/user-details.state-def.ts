import { UserController } from './user-details.controller';
import './user-details.html';
export function addUserDetailsState($stateProvider: angular.ui.IStateProvider) {
    $stateProvider.state('app.userdetails', {
        url: 'user-details',
        templateUrl: 'app/user/user-details.html',
        controller: UserController,
        controllerAs: '$ctrl'
    });
}
