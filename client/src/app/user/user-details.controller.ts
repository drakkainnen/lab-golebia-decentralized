import { AuthorizationService } from '../auth/authorization.service';

export class UserController {

    private userdetails:IUser = <IUser>{
        name: 'admin',
        username: 'admin',
        email: 'admin@admin.com',
        lastname: 'admin'
    };

    constructor(private authorizationService: AuthorizationService) {
        // if(!authorizationService.authenticated) {
        //     this.authorizationService.authenticate(null);
        // }
        // authorizationService;
    }


}

export interface IUser {
    name: string;
    lastname: string;
    username: string;
    email: string;
}