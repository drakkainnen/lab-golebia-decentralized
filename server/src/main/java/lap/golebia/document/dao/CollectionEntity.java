package lap.golebia.document.dao;


import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lap.golebia.document.Collection;
import lap.golebia.document.User;

@Entity
@Table(name = "collection")
public class CollectionEntity implements Collection {

	@Id
	@GeneratedValue
	private Long id;

	@Column(nullable = false)
	private String name;

	private String description;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id", nullable = false)
	private UserEntity user;
	
	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "collections")
	private Set<BirdEntity> birds;

	@Column(name="user_id", updatable = false, insertable = false)
	private Long userId;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public void setDescription(String description) {
		this.description = description;
	}

	public User getUser() {
		return user;
	}

	public void setUser(UserEntity userEntity) {
		this.user = userEntity;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		UserEntity user = new UserEntity();
		user.setId(userId);
		this.user = user;
		this.userId = userId;
	}

	public Set<BirdEntity> getBirds() {
		return birds;
	}

	public void setBirds(Set<BirdEntity> birds) {
		this.birds = birds;
	}
	
	
}
