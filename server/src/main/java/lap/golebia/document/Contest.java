package lap.golebia.document;

import java.util.List;

import org.springframework.data.annotation.Id;

public class Contest {
	@Id
	private String id;
	private String name;
	private List<Winner> winners;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<Winner> getWinners() {
		return winners;
	}

	public void setWinners(List<Winner> winners) {
		this.winners = winners;
	}

}
