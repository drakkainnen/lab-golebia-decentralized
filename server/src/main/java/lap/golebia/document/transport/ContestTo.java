package lap.golebia.document.transport;

import java.util.List;

import lap.golebia.document.Winner;

public class ContestTo {

	private String id;
	private String name;
	private List<Winner> winners;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Winner> getWinners() {
		return winners;
	}

	public void setWinners(List<Winner> winners) {
		this.winners = winners;
	}

}
