import { UserPasswordController } from './user-password.controller';
import './user-password.html';
export const userpasswordComponent: angular.IComponentOptions = {
    templateUrl: 'app/user/user-password/user-password.html',
    controller: UserPasswordController
};
