package lap.golebia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
public class LapGolebiaApplication {

	public static void main(String[] args) {
		SpringApplication.run(LapGolebiaApplication.class, args);
	}
}
