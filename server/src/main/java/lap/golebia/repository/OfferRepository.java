package lap.golebia.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import lap.golebia.document.Offer;

public interface OfferRepository extends MongoRepository<Offer, String>{
	public Offer findOneByIdAndUsername(String id, String username);
	public List<Offer> findAllByUsername(String name);
	public Offer findOneByBirdId(String id);
}
