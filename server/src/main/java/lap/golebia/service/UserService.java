package lap.golebia.service;

import static java.util.Optional.ofNullable;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lap.golebia.document.dao.UserEntity;
import lap.golebia.document.transport.UserTo;
import lap.golebia.repository.UserRepository;
import lap.golebia.security.SecUser;
import ma.glasnost.orika.MapperFacade;

@Service
public class UserService extends AbstractService {

	private final UserRepository userRepository;
	private final MapperFacade mapper;

	@Autowired
	public UserService(UserRepository userRepository, MapperFacade mapperFacade) {
		this.userRepository = userRepository;
		this.mapper = mapperFacade;
	}

	public UserTo getLoggedUserDetails() {
		Optional<UserEntity> result = ofNullable(userRepository.findOne(this.getUserId()));
		result.ifPresent(u -> u.setPassword(null));
		return result.map(u -> this.mapper.map(u, UserTo.class)).get();
	}

	public boolean updatePassword(String old, String newPasswd) {
		UserEntity user = userRepository.findOne(this.getUserId());
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		boolean result = false;
		if (encoder.matches(old, user.getPassword())) {
			user.setPassword(encoder.encode(newPasswd));
			userRepository.save(user);
			result = true;
		}
		return result;
	}

	public SecUser getSecUser(String username) {
		Optional<UserEntity> user = ofNullable(userRepository.findByUsername(username));
		return user.map(e -> new SecUser(e))
				.orElse(null);
	}

	public UserTo createNewUser(UserTo userTo) {
//		Set<UserEntity> users = userRepository.findByUsernameAndEmail(userTo.getUsername(), userTo.getEmail());
//		if(!users.isEmpty()) {
//			return null;
//		}
		// TODO: might throw an Unique Constraint exception 
		UserEntity user = mapper.map(userTo, UserEntity.class);
		return mapper.map(userRepository.save(user), UserTo.class);
	}

	@Transactional
	public UserTo updateUser(UserTo userTo) {
		UserEntity user = mapper.map(userTo, UserEntity.class);
		user.setFirstname(userTo.getFirstname());
		user.setLastname(userTo.getLastname());
		user.setDepartment(userTo.getDepartment());
		user.setLatitude(userTo.getLatitude());
		user.setLongtitude(userTo.getLongtitude());
		return mapper.map(userRepository.save(user), UserTo.class);
	}

}
