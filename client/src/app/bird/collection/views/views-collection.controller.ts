import { CollectionsService } from '../../common/collections.service';
import { BirdsService } from '../../common/birds.service';
import { ICollection } from '../collections.controller';
import { IBird } from '../../item/item-bird.controller';
export class ViewsCollectionController {

    private dragulaOptions: any = {
        revertOnSpill: true,
        copy: false,
        accepts: function (e: Element, from: Element, target: Element, sibling: Element) {
            return sibling;
        },
        moves: (e: Element, el, t, s) => {
            if (e.classList.contains('no-drag')) {
                return false;
            }
            return true;
        }
    };

    private readonly BIRDS_BAG: string = 'first-bag';

    constructor(private dragulaService: any, private $scope: any, private birdsService: BirdsService, private collectionsService: CollectionsService) {
        dragulaService.options($scope, this.BIRDS_BAG, this.dragulaOptions);

        $scope.$on('first-bag.drop', (e, el, targetContainer, sourceContainer) => {
            var targetId = Number(targetContainer.attr('id'));
            var sourceId = Number(sourceContainer.attr('id'));
            var birdRing = el.attr('id');
            this.removeDuplicates(targetId, sourceId, birdRing);
        });

        // birdsWithCollections.forEach((value: string) => {
        //     var key: number = Number(value);
        //     var birdCollection = this.birdsInCollection.get(key);
        //     if (!birdCollection) {
        //         this.birdsInCollection.set(key, []);
        //     }
        // });
    }

    getCollections():ICollection[] {
        return this.collectionsService.getSelectedCollections();
    }

    getBirdsForCollecton(collection: number): IBird[] {
        return this.birdsService.getBirdsForCollection(collection);
    }

    isCollectionEmpty(collectionNr: number): boolean {
        var collection = this.birdsService.getBirdsForCollection(collectionNr);
        if (collection) {
            return collection.length ? false : true;
        }
        return true;
    }

    delete(collection: number, bird: IBird) {
        // var idx = this.birdsInCollection.get(collection).indexOf(bird);
        // if (idx > -1) {
        //     this.birdsInCollection.get(collection).splice(idx, 1);
        // }
    }

    selectBird(bird: IBird) {
        bird.selected = !bird.selected;
    }

    toggleCopy() {
        this.dragulaOptions.copy = !this.dragulaOptions.copy;
    }

    isCopying() {
        return this.dragulaOptions.copy;
    }

    save() {
        this.birdsService.updateAllAtOnce();
    }


    private removeDuplicates(targetId: number, sourceId: number, birdRing: string) {
        if (targetId !== sourceId) {
            var birds = this.birdsService.getBirdsForCollection(targetId);
            if (birds) {
                var birdIndex = birds.findIndex((bird: IBird) => {
                    return bird.ring === birdRing;
                });
                this.deleteBird(targetId, birdIndex);
            }
        }
    }

    private deleteBird(collectionKey: number, birdIndex: number) {
        if (birdIndex > -1) {
            var birds = this.birdsService.getBirdsForCollection(collectionKey);
            birds.splice(birdIndex, 1);
        }
    }
}
