package lap.golebia.document.transport;

import java.util.Set;

import javax.validation.constraints.NotNull;

import lap.golebia.document.Bird;
import lap.golebia.document.common.BirdColor;

public class BirdTo implements Bird {

	private Long id;

	@NotNull
	private String ring;
	
	private BirdColor color;
	
	private Boolean gender;
	
	private Set<CollectionTo> collections;
	
	private Long userId;

	
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String getRing() {
		return this.ring;
	}

	@Override
	public void setRing(String ring) {
		this.ring = ring;
	}

	@Override
	public BirdColor getColor() {
		return color;
	}

	@Override
	public void setColor(BirdColor color) {
		this.color = color;
	}

	@Override
	public Long getUserId() {
		return this.userId;
	}

	@Override
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	public Set<CollectionTo> getCollections() {
		return collections;
	}

	public void setCollections(Set<CollectionTo> collections) {
		this.collections = collections;
	}

	@Override
	public Boolean getGender() {
		return this.gender;
	}

	@Override
	public void setGender(Boolean gender) {
		this.gender = gender;
	}
}
