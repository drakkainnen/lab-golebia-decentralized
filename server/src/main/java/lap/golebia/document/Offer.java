package lap.golebia.document;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;

public class Offer {

	@Id
	private String id;
	@DBRef
	private Bird bird;
	private String username;
	private List<String> comments;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Bird getBird() {
		return bird;
	}

	public void setBird(Bird bird) {
		this.bird = bird;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<String> getComments() {
		if(comments == null)
			comments = new ArrayList<>();
		return comments;
	}

	public void setComments(List<String> comments) {
		this.comments = comments;
	}

}
