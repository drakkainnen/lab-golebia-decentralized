/// <reference path="../../typings/index.d.ts" />

import * as angular from 'angular';
import 'angular-mocks';
import {appComponent} from './app.component';

describe('hello component', () => {
  beforeEach(() => {
    angular
      .module('fountainHello', ['app/hello.html'])
      .component('fountainHello', appComponent);
    angular.mock.module('fountainHello');
  });
  it('should render hello world', angular.mock.inject(($rootScope: ng.IRootScopeService, $compile: ng.ICompileService) => {
    const element = $compile('<fountain-hello>Loading...</fountain-hello>')($rootScope);
    $rootScope.$digest();
    const h1 = element.find('div');
    expect(h1.html()).toEqual('name');
  }));
});
