import { IBird } from '../item/item-bird.controller';
import { HookResult, ng1ViewsBuilder } from 'angular-ui-router/release/ng1';
import { ICollection } from '../collection/collections.controller';

export class BirdDomainRestService {

    constructor(private $http: angular.IHttpService, private rootUrl: string) {
    }

    createCollection(collection: ICollection): ng.IPromise<ICollection> {
        return this.$http.post(this.rootUrl + "service/collection", collection)
            .then(function (response: ng.IHttpPromiseCallbackArg<ICollection>) {
                return response.data;
            });
    }

    deleteCollection(collectionId: number): ng.IPromise<number> {
        return this.$http.delete(this.rootUrl + "service/collection/" + collectionId)
            .then(function (response: ng.IHttpPromiseCallbackArg<{}>) {
                return response.status;
            });
    }


    getBirdsToCollection(collectionsIds: number[]): ng.IPromise<Map<number, IBird[]>> {
        return this.$http.post(this.rootUrl + "service/collections/birds", collectionsIds)
            .then(function (response: ng.IHttpPromiseCallbackArg<Map<number, IBird[]>>) {
                return response.data;
            });
    }

    getCollections(): ng.IPromise<ICollection[]> {
        return this.$http.get(this.rootUrl + "service/collections")
            .then(function (response: ng.IHttpPromiseCallbackArg<ICollection[]>) {
                return response.data;
            });
    }

    getCollectionForId(number: number): ng.IPromise<ICollection[]> {
        return this.$http.get(this.rootUrl + "service/collection/" + number)
            .then(function (response: ng.IHttpPromiseCallbackArg<ICollection[]>) {
                return response.data;
            });
    }


    getBirdsListById(id: number): ng.IPromise<IBird[]> {
        return this.$http.get(this.rootUrl + "service/collections/" + id + "/birds")
            .then(function (response: ng.IHttpPromiseCallbackArg<IBird[]>) {
                return response.data;
            });
    }


    getAllBirds(): ng.IPromise<IBird[]> {
        return this.$http.get(this.rootUrl + "service/birds")
            .then(function (response: ng.IHttpPromiseCallbackArg<IBird[]>) {
                return response.data;
            });
    }


    updateBird(bird: IBird): ng.IPromise<IBird> {
        return this.$http.put(this.rootUrl + "service/bird", bird)
            .then(function (response: ng.IHttpPromiseCallbackArg<IBird>) {
                return response.data;
            });
    }

    createBird(bird: IBird): ng.IPromise<IBird> {
        return this.$http.post(this.rootUrl + "service/bird", bird)
            .then(function (response: ng.IHttpPromiseCallbackArg<IBird>) {
                return response.data;
            });
    }


    getBird(id: number): ng.IPromise<IBird> {
        return this.$http.get(this.rootUrl + "service/bird/" + id)
            .then(function (response: ng.IHttpPromiseCallbackArg<IBird>) {
                return response.data;
            });
    }


    deleteBird(id: number): ng.IPromise<IBird> {
        return this.$http.delete(this.rootUrl + "service/bird/" + id)
            .then(function (response: ng.IHttpPromiseCallbackArg<IBird>) {
                return response.data;
            });
    }

    addAllBirdsToCollection(birdIds: number[], collectionid: number): ng.IPromise<IBird> {
        return this.$http.post(this.rootUrl + "service/collections/" + collectionid + "/birds", birdIds)
            .then(function (response: ng.IHttpPromiseCallbackArg<IBird>) {
                return response.data;
            });
    }

}