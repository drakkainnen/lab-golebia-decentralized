insert into user values (1, 'admin', 'admin', 'admin@admin.com', 'admin', 'admin', null, null, null);

insert into collection values (1, 'stare', 'old pigeons collection', 1);
insert into collection values (2, 'mlode', 'young pigeons collection', 1);
insert into collection values (3, 'mlode', 'young pigeons collection', 1);

insert into bird values (1, "PL-92-123-123-123", 1, null, 1);
insert into bird values (2, "PL-92-100-100-100", 2, 1, 1);
insert into bird values (3, "PL-92-100-100-101", 0, 1, 1);

insert into bird_collection values (1, 1);
insert into bird_collection values (1, 2);
insert into bird_collection values (2, 2);
insert into bird_collection values (3, 3);
insert into bird_collection values (3, 1);
