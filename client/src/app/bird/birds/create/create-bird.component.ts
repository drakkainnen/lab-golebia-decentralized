import { CreateBirdController } from './create-bird.controller';

export class CreateBirdComponent implements angular.IComponentOptions {

    public controller: any;
    public controllerAs: string;
    public templateUrl: string;
    public bindings: { [binding: string]: string };

    constructor() {
        this.templateUrl = 'app/bird/birds/create/create-bird.html';
        this.controller = CreateBirdController;
        this.controllerAs = '$ctrl';
        this.bindings = {
        }
    }
}
