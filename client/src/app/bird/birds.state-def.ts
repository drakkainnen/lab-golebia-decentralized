import './birds.html';
export function addBirdsState($stateProvider: angular.ui.IStateProvider) {
    $stateProvider.state('app.birds', {
        url: 'birds',
        templateUrl: 'app/bird/birds.html'
    });
}
