package lap.golebia.document;

public interface User {

	Long getId();

	void setId(Long id);

	String getUsername();

	void setUsername(String username);

	String getPassword();

	void setPassword(String password);

	String getEmail();

	void setEmail(String email);

	String getFirstname();

	void setFirstname(String firstname);

	String getLastname();

	void setLastname(String lastname);

	String getDepartment();

	void setDepartment(String department);

	Double getLatitude();

	void setLatitude(Double latitude);

	Double getLongtitude();

	void setLongtitude(Double longtitude);
}