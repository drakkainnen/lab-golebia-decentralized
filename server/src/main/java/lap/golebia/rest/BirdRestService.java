package lap.golebia.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;

import lap.golebia.document.transport.BirdTo;

public interface BirdRestService {

	@GetMapping(value = "/service/collections/{id}/birds")
	List<BirdTo> getBirdsListById(Long id);

	@GetMapping(value = "/service/birds")
	List<BirdTo> getAllBirds();

	@PutMapping(value = "/service/bird")
	BirdTo updateBird(BirdTo bird);

	@PostMapping(value = "/service/bird")
	BirdTo createBird(BirdTo bird);

	@GetMapping(value = "/service/bird/{id}")
	BirdTo getBird(Long id);

	@DeleteMapping(value = "/service/bird/{id}")
	void deleteBird(Long id);

	@PostMapping(value = "/service/collections/{id}/birds")
	void addAllBirdsToCollection(ArrayList<Long> birdIds, Long collectionId);

}