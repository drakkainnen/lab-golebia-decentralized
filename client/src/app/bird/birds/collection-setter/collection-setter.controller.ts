import { BirdsService } from '../../common/birds.service';
import { ICollection } from '../../collection/collections.controller';
import { IBird } from '../../item/item-bird.controller';
import { CollectionsService } from '../../common/collections.service';

export class CollectionSetterController implements angular.IComponentController {

    private birds: IBird[];
    constructor(private collectionsService: CollectionsService,
        private birdsService: BirdsService) {
    }

    createCollection() {
        var col = <ICollection>{};
        col.name = 'new';
        this.collectionsService.createCollection(col)
            .then((data: ICollection) => {
                return this.birdsService.addCollectionForBirds(this.birds, data);
            })
            .then((data:IBird) => {
                console.log('success');
            });

    }
}
