package lap.golebia.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import lap.golebia.document.transport.BirdTo;
import lap.golebia.document.transport.CollectionTo;

public interface CollectionRestService {

	@GetMapping(value = "/service/collections")
	List<CollectionTo> getBirdCollections();

	@GetMapping(value = "/service/collection/{id}")
	CollectionTo getBirdCollectionById(Long id);

	@PostMapping(value = "/service/collection")
	CollectionTo createCollection(CollectionTo birdCollection);

	@DeleteMapping(value = "/service/collection/{id}")
	void deleteBirdCollection(Long id);
	
	@PostMapping("/service/collections/birds")
	Map<Long, Set<BirdTo>> getCollectionsWithBirds(ArrayList<Long> ids);

}