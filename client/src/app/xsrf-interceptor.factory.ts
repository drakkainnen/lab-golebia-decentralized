export default XSRFInterceptor;
/** @ngInject */
function XSRFInterceptor($cookies, $log): ng.IHttpInterceptor {

  var XSRFInterceptor: ng.IHttpInterceptor = {};
  XSRFInterceptor.request = (config) => {
    var token = $cookies.get('XSRF-TOKEN');
    if (token) {
      config.headers['X-XSRF-TOKEN'] = token;
      $log.info("X-XSRF-TOKEN: " + token);
    }
    return config;
  };

  return XSRFInterceptor;
}