package lap.golebia.rest.impl;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import lap.golebia.document.transport.BirdTo;
import lap.golebia.rest.BirdRestService;
import lap.golebia.service.BirdService;

@RestController
public class BirdRestServiceImpl implements BirdRestService {

	@Autowired
	private BirdService birdService;

	@Override
	public List<BirdTo> getBirdsListById(@PathVariable("id") Long id) {
		return birdService.getByCollectionId(id);
	}

	@Override
	public BirdTo updateBird(@Valid @RequestBody BirdTo bird) {
		return birdService.update(bird);
	}

	@Override
	public BirdTo createBird(@Valid @RequestBody BirdTo bird) {
		return birdService.create(bird);
	}

	@Override
	public BirdTo getBird(@PathVariable("id") Long id) {
		return birdService.getById(id);
	}

	@Override
	public void deleteBird(@PathVariable("id") Long id) {
		birdService.deleteById(id);
	}

	@Override
	public List<BirdTo> getAllBirds() {
		return birdService.getAllBirdsForCurrentUser();
	}

	@Override
	public void addAllBirdsToCollection(@RequestBody ArrayList<Long> birdIds, @PathVariable("id") Long collectionId) {
		birdService.addBirdsToCollection(birdIds, collectionId);
	}
}
