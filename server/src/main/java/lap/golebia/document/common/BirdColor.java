package lap.golebia.document.common;

public enum BirdColor {
	BLUE, BLU_PST, BLUE_NAKR, BLUE_NAKR_PST, RED, BLACK, RED_PST, SZPAK, DARK, PLOWY, DARK_NAKR, BIALY
}
