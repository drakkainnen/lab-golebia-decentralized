import { CollectionsService } from '../common/collections.service';
export interface ICollection {
    id?: number;
    name: string;
    description: string;
    selected?: boolean;
}

export class CollectionController {

    /* @ng-inject */
    constructor(private $state: angular.ui.IStateService, private collectionsService: CollectionsService) {
    }

    public getCollections():ICollection[] {
        return this.collectionsService.getCollections();
    }

    public isCollectionEmpty(): boolean {
        return this.collectionsService.getCollections() && this.collectionsService.getCollections().length > 0 ? false : true;
    }

    public createCollection(value: ICollection) {
        this.collectionsService.createCollection(value);
    }

    public removeCollection(value: ICollection) {
        this.collectionsService.deleteCollection(value);
        if (this.$state.current.name === 'app.birds.collections.collection') {
            this.$state.go(this.$state.$current['parent'].name);
        }
    }

    public selectCollection(value: ICollection) {
        value.selected = !value.selected;
        var selected: Array<number> = this.getSelectedCollectionsIds();
        if (selected.length) {
            this.openView(selected);
        } else {
            this.closeView();
        }
    }

    getSelectedCollectionsIds(): number[] {
        return this.getSelectedCollections()
            .map((value: ICollection, index: number) => {
                return value.id;
            });
    }

    getSelectedCollections(): ICollection[] {
        return this.collectionsService.getSelectedCollections();
    }

    private openView(selected: Array<number>) {
        var params: string = selected.join(',');
        this.$state.go('app.birds.collections.collection', { collectionIds: params });
    }

    private closeView() {
        this.$state.go(this.$state.$current['parent'].name);
    }
}
