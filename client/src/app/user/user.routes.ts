import { addLoginState } from './login/login.state-def';
import { addSignUpState } from './signup/signup.state-def';
import { addUserDetailsState } from './user-details.state-def';
import * as angular from 'angular';

export function userRoutesConfig($stateProvider: angular.ui.IStateProvider) {
    addLoginState($stateProvider);
    addSignUpState($stateProvider);
    addUserDetailsState($stateProvider);
}
