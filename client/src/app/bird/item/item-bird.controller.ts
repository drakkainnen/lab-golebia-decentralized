export interface IBird {
    id?: number;
    ring: string;
    colour: string;
    description?: string;
    collections?: any;
    selected?: boolean;
}

export class ItemBirdController {
    private bird: IBird = {
        ring: 'sdas',
        colour: 's sda',
        description: ' asdasd'
    };

    select() {
        this.bird.selected = !this.bird.selected;
    }
}
