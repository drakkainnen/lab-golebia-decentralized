import './create-collection.html';
import { CreateCollectionController } from './create-collection.controller';
export const createcollectionComponent: angular.IComponentOptions = {
    templateUrl: 'app/bird/collection/create/create-collection.html',
    controller: CreateCollectionController,
    bindings: {
        onCreate: '&'
    }
};
